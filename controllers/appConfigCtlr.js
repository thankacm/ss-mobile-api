/**
 * appConfigCtlr
 *
 * @description :: Server-side logic for managing config APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   File remaned from appConfigDao to appConfigCtlr.
 *                      -   Folder renamed from dao to controllers.
 */

/**
 * Module dependencies.
 */
var _ = require('underscore');
var mongoose = require('mongoose');
var Appconfig = require('../models/appconfig');
var JSONDiff = require('../util/jsondiff').JSONDiff;
var constants = require('../util/constants');
var client = require('../server/client').client;
var Config = require('../configs/config.global');
var logger = require('../util/logger');
var aiLogger = require('../util/aiLogger');

var resData = function () {
};

/**
 * GET /configs
 * Returns configs data.
 */
exports.getAppConfig = function (req, res) {

    try {
        var mrktCd = req.params.mrktCd;
        var apiVersion = parseInt(req.params.apiVrsn);
        var mrktConfigType = req.params.mrktConfigType;
        var downloadType = req.params.downloadType;
        //var currDataVer = (typeof req.param('currDataVer') != 'undefined' && req.param('currDataVer') != null) ? parseInt(req.param('currDataVer')) : '';
        var currDataVer = (typeof req.query.currDataVer != 'undefined' && req.query.currDataVer != null) ? req.query.currDataVer : '';

        var condition = condition || {};
        condition.mrkt_cd = mrktCd;
        condition.api_version = apiVersion;
        if (currDataVer && downloadType != constants.URL.DOWNLOAD_TYPE_FULL) {
            condition.data_version = parseInt(currDataVer);
        }

        var data = data || {};
        if (downloadType == constants.URL.DOWNLOAD_TYPE_FULL) {

            var redisKey = 'configs_' + apiVersion + '_' + mrktCd;
            client.exists(redisKey, function (err, reply) {
                if (!err) {
                    if (reply === 1) {
                        client.get(redisKey, function (err, configsData) {
                            if (err) {
                                resData = {status: false, code: 'SSCONF001', message: 'Error while reading redis cache.'};
                                res.send(resData);
                            } else {
                                var redisConfigsData = JSON.parse(configsData);
                                if (typeof redisConfigsData != 'undefined' && redisConfigsData != null) {
                                    if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                                        data._id = redisConfigsData._id;
                                        data.lookup_id = redisConfigsData.lookup_id;
                                        data.api_version = redisConfigsData.api_version;
                                        data.data_version = redisConfigsData.data_version;
                                        data.mrkt_cd = redisConfigsData.mrkt_cd;
                                        data.configs = {};
                                        data.configs[mrktConfigType] = redisConfigsData.configs[mrktConfigType];
                                    } else {
                                        data = redisConfigsData;
                                    }
                                }
                                res.send(data);
                                res.end();
                            }
                        });
                    } else {
                        Appconfig.findOne(condition).lean().limit(1).sort({_id: -1}).exec(function (err, row) {
                            if (!err) {
                                if (typeof row != 'undefined' && row != null) {
                                    if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                                        data._id = row._id;
                                        data.lookup_id = row.lookup_id;
                                        data.api_version = row.api_version;
                                        data.data_version = row.data_version;
                                        data.mrkt_cd = row.mrkt_cd;
                                        data.configs = {};
                                        data.configs[mrktConfigType] = row.configs[mrktConfigType];
                                    } else {
                                        if (row && Object.keys(row).length != 0) {
                                            client.set(redisKey, JSON.stringify(row));
                                        }
                                        data = row;
                                    }
                                }
                                res.send(data);
                                res.end();
                            } else {
                                res.send({success: false, message: 'Error while fetching app configurations.'});
                                res.end();
                            }
                        });
                    }
                }
            });

        } else if (downloadType == constants.URL.DOWNLOAD_TYPE_DELTA) {

            /* Delta logic */
            Appconfig.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, oldDoc) {
                delete condition.data_version;
                Appconfig.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, newDoc) {
                    var oldData = oldData || {};
                    var newData = newData || {};
                    if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                        oldData._id = oldDoc._id;
                        oldData.configs = {};
                        oldData.configs[mrktConfigType] = oldDoc.configs[mrktConfigType];

                        newData._id = newDoc._id;
                        newData.configs = {};
                        newData.configs[mrktConfigType] = newDoc.configs[mrktConfigType];
                    } else {
                        oldData = oldDoc;
                        newData = newDoc;
                    }

                    if ((oldData._id).toString() !== (newData._id).toString()) {
                        delete oldData._id;
                        if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                            delete newData._id;
                        }
                        var delta = JSONDiff.delta(oldData, newData);

                        if (delta.hasOwnProperty('configs') && (delta.configs).hasOwnProperty('onboardingFields') && typeof delta.configs.onboardingFields != 'undefined') {
                            var data = _(delta.configs.onboardingFields).map(function (g, key) {
                                return delta.configs.onboardingFields[key];
                            });
                            delta.configs.onboardingFields = data;
                        }

                        if (delta.hasOwnProperty('configs') && (delta.configs).hasOwnProperty('pickupAddrFields') && typeof delta.configs.pickupAddrFields != 'undefined') {
                            var data = _(delta.configs.pickupAddrFields).map(function (g, key) {
                                return delta.configs.pickupAddrFields[key];
                            });
                            delta.configs.pickupAddrFields = data;
                        }

                        delta = (delta == false) ? {} : delta;
                        res.send(delta);
                        res.end();
                    } else {
                        res.send({});
                        res.end();
                    }
                });
            });

        }
    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'Configs - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};














