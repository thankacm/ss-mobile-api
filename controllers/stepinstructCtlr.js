/**
 * stepinstructCtlr
 *
 * @description :: Server-side logic for managing service version APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   File remaned from svcversionsDao to svcversionsCtlr.
 *                      -   Folder renamed from dao to controllers.
 */

/**
 * Module dependencies.
 */
var _ = require('underscore');
var mongoose = require('mongoose');
var SSAppFeaturesAPIData = require('../models/SSAppFeaturesAPIData');
var Config = require('../configs/config.global');
var client = require('../server/client').client;
var logger = require('../util/logger');
var JSONDiff = require('../util/jsondiff').JSONDiff;
var constants = require('../util/constants');
var aiLogger = require('../util/aiLogger');

var resData = function () {
};

/**
 * GET /stepinstruct
 * Returns AvonCauses details.
 */
exports.getStepInstruct = function (req, res) {

    try {
        var mrktCd = req.params.mrktCd;
        var langCd = req.params.langCd;
        var apiVersion = parseInt(req.params.apiVrsn);
        var downloadType = req.params.downloadType;
        var currDataVer = (typeof req.query.currDataVer != 'undefined' && req.query.currDataVer != null) ? req.query.currDataVer : '';

        var data = data || {};
        var condition = condition || {};
        condition.api_version = apiVersion;
        //condition.api_type = 'step_instruct';
        condition.api_type = 'stepbystep';
        condition.lang_cd = langCd;
        condition.mrkt_cd = mrktCd;
        if (currDataVer && downloadType != constants.URL.DOWNLOAD_TYPE_FULL) {
            condition.data_version = parseInt(currDataVer);
        }

        if (downloadType == constants.URL.DOWNLOAD_TYPE_FULL) {

            SSAppFeaturesAPIData.findOne(condition, {api_type: 0}).lean().sort({_id: -1}).limit(1).exec(function (err, row) {
                if (!err) {
                    if (typeof row != 'undefined' && row != null) {
                        data = row;
                    }
                    res.send(data);
                    res.end();
                } else {
                    res.send({success: false, message: 'Error while fetching StepByStep.'});
                    res.end();
                }
            });

        } else if (downloadType == constants.URL.DOWNLOAD_TYPE_DELTA) {

            /* Delta logic */
            SSAppFeaturesAPIData.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, oldDoc) {
                delete condition.data_version;
                SSAppFeaturesAPIData.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, newDoc) {
                    var oldData = oldData || {};
                    var newData = newData || {};

                    oldData = oldDoc;
                    newData = newDoc;

                    if (typeof oldData != 'undefined' && oldData != null && oldData.hasOwnProperty('_id')) {
                        if ((oldData._id).toString() !== (newData._id).toString()) {
                            delete oldData._id;

                            var changedStepByStepArr = [];
                            (newData.stepbystep).forEach(function (newStep) {
                                var hasMatch = JSONDiff.checkValueExists(oldData.stepbystep, 'instruction_id', newStep.instruction_id);
                                if (hasMatch) {
                                    (oldData.stepbystep).forEach(function (oldStep) {
                                        if (newStep.instruction_id === oldStep.instruction_id) {
                                            var delta = JSONDiff.delta(oldStep, newStep);
                                            if (delta !== false) {
                                                changedStepByStepArr.push(newStep);
                                            }
                                        }
                                    });
                                } else {
                                    changedStepByStepArr.push(newStep);
                                }
                            });

                            var stepByStepRes = stepByStepRes || {};
                            stepByStepRes.stepbystep = changedStepByStepArr;

                            res.send(stepByStepRes);
                            res.end();
                        } else {
                            res.send({});
                            res.end();
                        }
                    } else {
                        res.send({message: 'Invalid data version.'});
                        res.end();
                    }
                });
            });
        }

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'StepByStep - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};












