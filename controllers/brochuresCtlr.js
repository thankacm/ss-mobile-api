/**
 * brochuresCtlr
 *
 * @description :: Server-side logic for managing brochure data APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   File remaned from brochureDataDao to brochureDataCtlr.
 *                      -   Folder renamed from dao to controllers.
 *              0.5     -   One collection different documents.
 *                      -   Url changed for getBrochureList.
 */

/**
 * Module dependencies.
 */
var _ = require('underscore');
var mongoose = require('mongoose');
var SSAppBrochuresAPIData = require('../models/SSAppBrochuresAPIData');
var Config = require('../configs/config.global');
var client = require('../server/client').client;
var logger = require('../util/logger');
var camelcaseKeys = require('camelcase-keys');
var aiLogger = require('../util/aiLogger');
/**
 * GET /brochurelist
 * Returns brochure list.
 */
exports.getBrochureList = function (req, res) {
    try {
        var apiVersion = parseInt(req.params.apiVrsn);
        var mrktCd = req.params.mrktCd;
        var langCd = req.params.langCd;
        var campYrNr = req.param('campYrNr');
        var data = data || {};
        campYrNr = JSON.parse(campYrNr);
        var cLength = campYrNr.length;

        //Model.find({$or: [
        //        {$and: [{campaign_year: 17}, {campaign_num: 7}]},
        //        {$and: [{campaign_year: 17}, {campaign_num: 11}]}]
        //});

        var orArr = [];
        for (var i = 0; i < cLength; i++) {
            var year = parseInt(campYrNr[i].slice(2, 4));
            var num = parseInt(campYrNr[i].slice(4, 6));

            var andArr = [];
            var orBbj = {};
            var andObj = {};
            andObj.campaign_year = year;
            andArr.push(andObj);
            andObj = {};
            andObj.campaign_num = num;
            andArr.push(andObj);

            orBbj.$and = andArr;
            orArr.push(orBbj);
        }

        //console.log('orArr');
        //console.log(JSON.stringify(orArr));
        // Brochure.cmpgn_yr_nr = bchr.campaign_year + bchr.campaign_num;

        if (!campYrNr || typeof campYrNr == 'undefined' || campYrNr.length <= 0) {
            res.send({success: false, message: 'Invalid campYrNr query parameters.'});
            res.end();
        } else {
            //var condition = {'mrkt_cd': mrktCd, 'api_version': apiVersion, 'api_type': 'brochure_list', 'lang_cd': langCd, 'cmpgn_yr_nr': {$in: campYrNr}};
            //var projection = {_id: 0, lookup_id: 0, api_type: 0, api_version: 0, data_version: 0, mrkt_cd: 0, lang_cd: 0, campaign_year: 0, campaign_num: 0, cmpgn_yr_nr: 0};

            var condition = {'mrkt_cd': mrktCd, 'lang_cd': langCd, 'api_version': apiVersion, 'api_type': 'brochure_list', $or: orArr};
            var projection = {_id: 0, lookup_id: 0, api_type: 0, api_version: 0, data_version: 0, mrkt_cd: 0, lang_cd: 0, campaign_year: 0, campaign_num: 0, brochure_num: 0};
            SSAppBrochuresAPIData.find(condition, projection).lean().sort({_id: -1}).exec(function (err, rows) {
                if (!err) {
                    var tmpDataArr = [];
                    if (typeof rows != 'undefined' && rows != null) {
                        rows.forEach(function (tData, index) {
                            tmpDataArr.push(camelcaseKeys(tData.brochures, {deep: true}))
                        });
                    }
                    data.brochures = tmpDataArr;
                    res.send(data);
                    res.end();
                } else {
                    res.send({success: false, message: 'Error while fetching brochure list.'});
                    res.end();
                }
            });
        }

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'Service version - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};
/**
 * GET /brochuredata
 * Returns brochure data.
 */
exports.getBrochureData = function (req, res) {
    try {
        var apiVersion = parseInt(req.params.apiVrsn);
        var mrktCd = req.params.mrktCd;
        var langCd = req.params.langCd;
        var cmpgnYr = parseInt(req.params.cmpgnYr);
        var cmpgnNr = parseInt(req.params.cmpgnNr);
        var brochureNr = parseInt(req.params.brochureNr);
        var data = data || {};
        //var condition = {'mrkt_cd': mrktCd, 'api_version': apiVersion, 'api_name': 'BrochureData', 'lang_cd': langCd, 'cmpgn_yr': cmpgnYr, 'cmpgn_nr': cmpgnNr, 'brochure_nr': brochureNr};
        var condition = {'mrkt_cd': mrktCd, 'api_version': apiVersion, 'api_type': 'brochure_data', 'lang_cd': langCd, 'campaign_year': cmpgnYr, 'campaign_num': cmpgnNr, 'brochure_num': brochureNr};
        var projection = {api_type: 0};
        SSAppBrochuresAPIData.findOne(condition, projection).lean().limit(1).sort({_id: -1}).exec(function (err, row) {
            if (!err) {
                if (typeof row != 'undefined' && row != null) {
                    data = row;
                }
                var tempID = data._id;
                delete data._id;
                var data = camelcaseKeys(data, {deep: true});
                data._id = tempID;
                res.send(data);
                res.end();
            } else {
                res.send({success: false, message: 'Error while fetching brochure data.'});
                res.end();
            }
        });
    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'Service version - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};