/**
 * markets model
 *
 * @description :: Represents data, implements business logic and handles storage
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

/**
 * Defining markets schema Model in mongoose.
 */
var marketsSchema = new mongoose.Schema({
}, {
    collection: 'markets'
});

/**
 * Exports markets model object.
 */
module.exports = mongoose.model('markets', marketsSchema);
