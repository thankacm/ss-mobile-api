/**
 * translations model
 *
 * @description :: Represents data, implements business logic and handles storage
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

/**
 * Defining translations schema Model in mongoose
 */
var translationsSchema = new mongoose.Schema({
}, {
    collection: 'translations'
});

/**
 * Exports translations model object.
 */
module.exports = mongoose.model('translations', translationsSchema);
