/**
 * Constants
 *
 * @description :: Defines constants
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

module.exports = {
    URL: {
        MRKT_CONFIG_TYPE_ALL: 'all',
        DOWNLOAD_TYPE_FULL: 'full',
        DOWNLOAD_TYPE_DELTA: 'delta'
    }
};