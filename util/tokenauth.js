/**
 * Tokenauth 
 *
 * @description :: Hnadles token authentication
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Handled unauthorized requests (401).
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var Apikeys = require('../models/apikeys');
var config = require('../configs/config.global');

var TokenAuth = function () {
};

/**
 * Method : validateToken
 * Description : Validates devkey.
 */
TokenAuth.prototype.validateToken = function (req, res, next) {
    var devkey = req.headers['devkey'];
    if (devkey !== config.devkey || !devkey || typeof devkey == 'undefined') {
        res.status(401).send({success: false, type: 'tokencheck', errorMessage: 'Invalid Dev Key.'});
        res.end();
    } else {
        next();
    }
};

TokenAuth.prototype.validateToken_BK = function (req, res, next) {
    var devkey = req.headers['devkey'];

    Apikeys.findOne({devkey: devkey}).lean().exec(function (err, row) {
        if (!err) {
            if (row && typeof row.devkey != 'undefined' && devkey === row.devkey) {
                next();
            } else {
                res.status(401).send({success: false, type: 'tokencheck', errorMessage: 'Invalid Dev Key.'});
                res.end();
            }
        } else {
            res.status(401).send({success: false, type: 'tokencheck', errorMessage: 'Invalid Dev Key.'});
            res.end();
        }
    });
};

/**
 * Exports TokenAuth object. 
 */
module.exports.TokenAuth = new TokenAuth();