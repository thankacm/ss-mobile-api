/**
 * Logger
 *
 * @description :: Handles error logging
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 */

/**
 * Module dependencies.
 */
var winston = require('winston');
var fs = require('fs');
var config = require('../configs/config.global');
var logDir = './logs';

/**
 * Configuring log levels & colors.
 */
winston.emitErrs = true;
winston.setLevels(config.logger.levels);
winston.addColors(config.logger.colors);

/**
 * Creating log directory if it does not exist.
 */
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

var tsFormat = (new Date()).toLocaleTimeString();

/**
 * Method to write logs.
 */
var logger = new winston.Logger({
    exitOnError: false,
    levels: config.logger.levels,
    transports: [
        //new (require('winston-daily-rotate-file'))({
        new (winston.transports.File)({
            filename: logDir + '/' + config.logger.logfile + '_error.log',
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: false,
            json: true,
            handleExceptions: true,
            level: 'error',
            name: 'logError'
        }),
        new (winston.transports.File)({
            filename: logDir + '/' + config.logger.logfile + '_warn.log',
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: false,
            json: true,
            level: 'warn',
            name: 'logWarn'
        }),
        new (winston.transports.File)({
            filename: logDir + '/' + config.logger.logfile + '_info.log',
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: false,
            json: true,
            level: 'info',
            name: 'logInfo'
        }),
        new (winston.transports.File)({
            filename: logDir + '/' + config.logger.logfile + '_debug.log',
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: false,
            json: true,
            level: 'debug',
            name: 'logDebug'
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ]

});

/**
 * Exports logger object.
 */
module.exports = logger;

/**
 * Exports stream object. Helps to handle uncaught exceptions.
 */
module.exports.stream = {
    write: function (message, encoding) {
        if (config.logger.logging) {
            //logger.info(message);
            //logger.debug(message);
            //logger.warn(message);
            logger.error(message);
        }
    }
};





