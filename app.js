/**
 * app.js
 *
 * @description :: Initializes the app and glues everything together
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   Added MongoDB connection pool.
 *                      -   Handled bad requests (400).
 */

/**
 * Module dependencies.
 */
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var brochureAPIs = require('./api_routes/brochures');
var configAPIs = require('./api_routes/config');
var translationAPIs = require('./api_routes/translations');
var marketAPIs = require('./api_routes/markets');
var svcVersionsAPIs = require('./api_routes/svcversions');
var helpsAPIs = require('./api_routes/helps');
var gamificationAPIs = require('./api_routes/gamification');
var sharecontentAPIs = require('./api_routes/sharecontent');
var faqAPIs = require('./api_routes/faq');
var avoncausesAPIs = require('./api_routes/avoncauses');
var stepinstructAPIs = require('./api_routes/stepinstruct');
var analyticsAPIs = require('./api_routes/analytics');
var cors = require('cors');
var mongoose = require('mongoose');
var secrets = require('./configs/config.mongo');
var logger = require('./util/logger');
var morgan = require('morgan');
var Config = require('./configs/config.global');


/**
 * Winston - Azure application insights.
 */
//var appInsights = require("applicationinsights");
//var aiLogger = require('winston-azure-application-insights').AzureApplicationInsightsLogger;
//var winston = require('winston');
//
//appInsights.setup(Config.insights.instrumentationKey)
//        .setAutoDependencyCorrelation(Config.insights.autoDependencyCorrelation)
//        .setAutoCollectRequests(Config.insights.autoCollectRequests)
//        .setAutoCollectPerformance(Config.insights.autoCollectPerformance)
//        .setAutoCollectExceptions(Config.insights.autoCollectExceptions)
//        .setAutoCollectDependencies(Config.insights.autoCollectDependencies)
//        .start();
/**
 * Use an existing app insights SDK instance 
 */
//winston.add(aiLogger, {
//    insights: appInsights
//});

/**
 * MongoDB connection.
 */
var connect = function () {
    mongoose.connect(secrets.db.mongo, {server: {poolSize: 20}}, function (err, res) {
        if (err) {
            console.log('Error connecting to: ' + secrets.db.mongo + '. ' + err);
            mongoose.disconnect();
        } else {
            console.log('Succeeded connected to: ' + secrets.db.mongo);
        }
    });
};
connect();
mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect);

var app = express();

/**
 * View engine setup.
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use('/static', express.static(path.join(__dirname, 'public')));

/**
 * Includes all the routes.
 */
app.use('/', brochureAPIs, configAPIs, translationAPIs, marketAPIs, svcVersionsAPIs, helpsAPIs, gamificationAPIs, sharecontentAPIs, faqAPIs, avoncausesAPIs, stepinstructAPIs, analyticsAPIs);

/**
 * Catch 400 and forward to error handler.
 */
app.all('*', function (req, res, next) {
    //throw new Error("Bad request")
    var err = new Error('Bad Request');
    err.status = 400;
    next(err);
});

/**
 * Handles uncaught exceptions.
 */
/* v0.2 - Begin : Exception handling changes. */
app.use(morgan('combined', {"stream": logger.stream}));
/* v0.2 - End : Exception handling changes. */

/**
 * Catch 404 and forward to error handler.
 */
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/**
 * Production error handler. No stacktraces leaked to user.
 */
/* v0.2 - Begin : Exception handling changes. */
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    var message;
    if (err.message == 'Bad Request') {
        message = 'Bad request.';
    } else if (err.message == 'Not Found') {
        message = 'Service not found.';
    } else {
        message = 'Internal server error.';
    }
    res.send({status: false, message: message});
    //res.status(err.status).send({status: false, message: message});
    res.end();
});
/* v0.2 - End : Exception handling changes. */

/**
 * Exports app object. 
 */
module.exports = app;
