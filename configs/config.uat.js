/**
 * UAT configurations.
 *
 * @description :: Defines UAT configurations
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var config = require('./config.global');

config.hostname = 'uat.mobilecms.avon.com';

/**
 * MySQL database settings.
 */
config.mysql = {};
config.mysql.uri = 'ryelxqfmobdb01.rye.avon.com';
config.mysql.user = 'hyfn_appidq';
config.mysql.password = '148fc78336216a74';
config.mysql.db = 'mobuatcms';
config.mysql.connectionLimit = 200;

/**
 * Exports config object.
 */
module.exports = config;