/**
 * Global configurations
 *
 * @description :: Defines Global configurations
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Create config object.
 */
var config = module.exports = {};

config.env = 'dev'; /* dev, uat, prod */
config.port = process.env.SSLCFG_API_PORT; /* 5037 */
config.devkey = process.env.SSLCFG_API_DEVKEY; /* f81376694e0d9822c5d29aaa92e02081 */

/**
 * Login settings.
 */
config.login = {};
config.login.secretkey = "developmentkey";

/**
 * Logger settings.
 */
config.logger = {};
config.logger.level = 'error'; /* error, warn, info, debug */
config.logger.logging = true; /* true, false */
config.logger.logfile = 'log'; /* log file name */
config.logger.levels = {error: 0, warn: 1, info: 2, debug: 3}; /* 0 - 3 : high - low */
config.logger.colors = {info: "green", warn: "yellow", error: "red"};

/**
 * Session settings.
 */
config.session = {};
config.session.lifetime = 20 * 60; /* 20 minutes */

/**
 * Redis server settings.
 */
config.redis = {};
config.redis.cacheExpiryTime = 1 * 60 * 60; /* In seconds */

/**
 * Winston - Azure application insights settings.
 */
config.insights = {};
config.insights.instrumentationKey = process.env.APPINSIGHTS_INSTRUMENTATIONKEY;
config.insights.autoDependencyCorrelation = true;
config.insights.autoCollectRequests = true;
config.insights.autoCollectPerformance = true;
config.insights.autoCollectExceptions = true;
config.insights.autoCollectDependencies = true;
