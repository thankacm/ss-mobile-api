/**
 * DEV configurations.
 *
 * @description :: Defines DEV configurations
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                          Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var config = require('./config.global');

config.hostname = 'dev.config.avon.com';

/**
 * MySQL database settings.
 */
config.mysql = {};
config.mysql.uri = 'ryelxmobddb1.rye.avon.com';
config.mysql.user = 'hyfn_appid';
config.mysql.password = '148fc78336216a74';
config.mysql.db = 'mobappd2';
config.mysql.connectionLimit = 200;

/**
 * Exports config object.
 */
module.exports = config;