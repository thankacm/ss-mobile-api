/**
 * ShareContent route
 *
 * @description :: Defines app routes for service versions 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var sharecontentCtlr = require('../controllers/sharecontentCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;

/**
 * ShareContent route.
 */
router.get('/:apiVrsn/sharingcontent/:mrktCd/:langCd/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    sharecontentCtlr.getSharingContent(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

