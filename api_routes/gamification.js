/**
 * Gamification route
 *
 * @description :: Defines app routes for service versions 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var gamificationCtlr = require('../controllers/gamificationCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;

/**
 * Gamification route.
 */
router.get('/:apiVrsn/gamification/:mrktCd/:langCd/:mrktConfigType/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    gamificationCtlr.getGamification(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

