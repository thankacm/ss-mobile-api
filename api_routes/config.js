/**
 * Configs route
 *
 * @description :: Defines app routes for configs 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var appConfigCtlr = require('../controllers/appConfigCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;

/**
 * Configs route.
 */
router.get('/:apiVrsn/configs/:mrktCd/:mrktConfigType/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    appConfigCtlr.getAppConfig(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

