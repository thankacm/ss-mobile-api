/**
 * Helps route
 *
 * @description :: Defines app routes for service versions 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var helpsCtlr = require('../controllers/helpsCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;

/**
 * Service versions route.
 */
router.get('/:apiVrsn/helps/:mrktCd/:langCd/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    helpsCtlr.getHelps(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

