/**
 * Translations route
 *
 * @description :: Defines app routes for translations
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var translationsCtlr = require('../controllers/translationsCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;

/**
 * Translations route.
 */
router.get('/:apiVrsn/translations/:mrktCd/:langCd/:mrktConfigType/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    translationsCtlr.getMarketTranslations(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

