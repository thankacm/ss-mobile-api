/**
 * Brochures route
 *
 * @description :: Defines app routes for brochures 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *              0.5     -   One collection different documents
 *                      -   Url changed for getBrochureList.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var brochuresCtlr = require('../controllers/brochuresCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;

/**
 * Brochure list route.
 */
router.get('/:apiVrsn/brochurelist/:mrktCd/:langCd', tokenAuth.validateToken, function (req, res, next) {
    brochuresCtlr.getBrochureList(req, res);
});

/**
 * Brochure data route.
 */
router.get('/:apiVrsn/brochuredata/:mrktCd/:langCd/:cmpgnYr/:cmpgnNr/:brochureNr', tokenAuth.validateToken, function (req, res, next) {
    brochuresCtlr.getBrochureData(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

