/**
 * Markets route
 *
 * @description :: Defines app routes for markets 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var marketsCtlr = require('../controllers/marketsCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;

/**
 * Markets route.
 */
router.get('/:apiVrsn/markets', tokenAuth.validateToken, function (req, res, next) {
    marketsCtlr.getMarketList(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

